import {ChallengeType} from './ChallengeType';
import {BageShapeType} from './BageShapeType';
import {ColorEnum} from './ColorEnum';
import {User} from './User';

export class Challenge {
  id: string;
  type: ChallengeType;
  bageShape: BageShapeType;
  exp: number;
  color: ColorEnum;
  name: string;
  title: string;
  participantsIds: string[];
  dateTo: Date;
  participants: User[];
  img: string;
}
