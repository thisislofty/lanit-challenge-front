export enum BageShapeType {
  CIRCLE,
  SQUARE,
  STAR,
  SHIELD,
  MEDAL,
  PENTAGON,
  HEXAGON,
  OCTAGON
}
