import {Challenge} from './Challenge';

export class User {
  id: string;
  name: string;
  photo: string;
  level: number;
  exp: number;
  isMaster: boolean;
  achievements: Challenge[];
  activeChallenges: Challenge[];
  createdChallenges: Challenge[];
}
