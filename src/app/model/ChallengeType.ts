export enum ChallengeType {
  WORK = 'WORK',
  EDUCATION = 'EDUCATION',
  SPORT = 'SPORT',
  FUN = 'FUN',
  MENTORSHIP = 'MENTORSHIP',
  OTHER = 'OTHER'
}
