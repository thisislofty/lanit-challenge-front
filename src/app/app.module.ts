import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ClarityModule, ClrCommonFormsModule, ClrFormsModule, ClrFormsNextModule, ClrInputContainer, ClrInputModule} from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './main/main.component';
import {ValuesPipe} from './main/ValuesPipe';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ChallengesComponent } from './challenges/challenges.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ValuesPipe,
    ChallengesComponent
  ],
  imports: [
    FormsModule,
    ClarityModule,
    ClrFormsNextModule,
    ClrInputModule,
    ClrFormsModule,
    ClrCommonFormsModule,
    BrowserModule,
    ClarityModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
