import {Component, Injectable, OnInit} from '@angular/core';
import {ChallengeService} from '../challenge.service';
import {Challenge} from '../model/Challenge';
import {UserService} from '../user.service';
import {element} from 'protractor';
import {ChallengeType} from '../model/ChallengeType';
import {User} from '../model/User';

@Component({
  selector: 'app-challenges',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.css']
})
@Injectable()
export class ChallengesComponent implements OnInit {
  str: string;
  openCreateModal: boolean;
  model: string;
  n: string;
  file: File;
  private imgURL: any;
  private img: any;
  private imagePath: FileList;
  private message: string;
  description: string;
  private numbers: number[];
  private challengeToCreate: Challenge;
  openViewModal: boolean;
  challengeToView: Challenge;
  private challenges: Challenge[];

  constructor(private challengeService: ChallengeService, private userService: UserService) {
    // tslint:disable-next-line:max-line-length
    this.str = 'Главный герой — 27-летний Илья Горюнов, семь лет отсидевший в тюрьме по ложному обвинению в распространении наркотиков. Когда Илья выходит на свободу, он понимает, что прежняя жизнь, по которой он тосковал, разрушена, и вернуться к ней он больше не сможет. Хотя он не собирался мстить человеку, который отправил его в тюрьму, другого выхода теперь нет. Встретившись лицом к лицу со своим обидчиком, Илья совершает необдуманный поступок, после которого главный герой получает доступ к смартфону Петра, а с ним и к жизни молодого человека — его фотографиям и видео, перепискам с родителями и девушкой Ниной, к странным, полным недомолвок и угроз переговорам с коллегами. На время Илья становится для всех Петром — через текст на экране телефона.';
  }

  ngOnInit() {
    this.challengeToView = new Challenge();
    this.challengeToCreate = new Challenge();
    // @ts-ignore
    this.numbers = Array(9).fill().map((x, i) => i); // [0,1,2,3,4]
    this.challenges = new Array<Challenge>();
    this.challengeService.list().subscribe(data => {
      // tslint:disable-next-line:no-shadowed-variable
      this.challenges = data;
    });
  }

  handleFileInput(files: FileList) {
    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    this.challengeToCreate.img = files[0].name;

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }

  createChallenge() {
    // this.openCreateModal = false;
    const chellenge = new Challenge();
    chellenge.name = this.challengeToCreate.name;
    chellenge.title = this.challengeToCreate.title;
    chellenge.type = this.challengeToCreate.type;
    chellenge.img = this.challengeToCreate.img;
    chellenge.dateTo = this.challengeToCreate.dateTo;
    chellenge.exp = this.challengeToCreate.exp;
    this.challengeService.createChallenge(chellenge).subscribe(data => this.openCreateModal = false);
  }

  challegeTypes(): ChallengeType[] {
    // tslint:disable-next-line:max-line-length
    return [ChallengeType.EDUCATION, ChallengeType.WORK, ChallengeType.SPORT, ChallengeType.FUN, ChallengeType.MENTORSHIP, ChallengeType.OTHER];
  }

  openChallenge(id: string): void {
    this.challengeService.get(id).subscribe(data => this.challengeToView = data);
  }

  cancelCreation() {
    this.openCreateModal = false;
    this.challengeToCreate = new Challenge();
  }

  getParticipants(participantsIds: string[]): User[] {
    return this.challengeToView.participants;
  }

  takeChallenge() {
    this.userService.takeChallenge(this.challengeToView.id).subscribe(data => this.openViewModal = false);
    this.openViewModal = false;
  }

  closeViewModal() {
    this.challengeToView = new Challenge();
    this.openCreateModal = false;
  }

  getChallenges(type: ChallengeType): Challenge[] {
    // tslint:disable-next-line:no-shadowed-variable
    return this.challenges.filter(element => element.type === type);
  }

  getChallengesEducation(): Challenge[] {
    return this.getChallenges(ChallengeType.EDUCATION);
  }

  getChallengesWork(): Challenge[] {
    return this.getChallenges(ChallengeType.WORK);
  }

  getChallengesSport(): Challenge[] {
    return this.getChallenges(ChallengeType.SPORT);
  }

  getChallengesFun(): Challenge[] {
    return this.getChallenges(ChallengeType.FUN);
  }

  getChallengesMentorship(): Challenge[] {
    return this.getChallenges(ChallengeType.MENTORSHIP);
  }

  getChallengesOther(): Challenge[] {
    return this.getChallenges(ChallengeType.OTHER);
  }

  openView(id: string): void {
    this.openViewModal = true;
    this.challengeService.get(id).subscribe(data => {
      this.challengeToView = data;
      this.userService.list(this.challengeToView.participantsIds).subscribe(users => this.challengeToView.participants = users);
    });
  }
}
