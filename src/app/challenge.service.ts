import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Challenge} from './model/Challenge';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {

  private url = 'http://localhost:8080/challenge';

  constructor(private http: HttpClient) {
  }

  createChallenge(challenge: Challenge): Observable<boolean> {
    return this.http.post<boolean>(this.url + '/create', challenge);
  }

  get(id: string): Observable<Challenge> {
    return this.http.get<Challenge>(this.url + '?id=' + id);
  }

  list(): Observable<Challenge[]> {
    return this.http.get<Challenge[]>(this.url + '/list');
  }
}
