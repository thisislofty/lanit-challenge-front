import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Challenge} from './model/Challenge';
import {User} from './model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'http://localhost:8080/user';

  constructor(private http: HttpClient) {
  }

  get(id: string): Observable<User> {
    return this.http.post<User>(this.url, id);
  }

  list(ids: string[]): Observable<User[]> {
    return this.http.post<User[]>(this.url, ids);
  }

  takeChallenge(challengeId: string): Observable<boolean> {
    return this.http.get<boolean>(this.url + '/take-challenge?challengeId=' + challengeId);
  }

  listChallenges(): Observable<Challenge[]> {
    return this.http.get<Challenge[]>(this.url + '/challenges');
  }
}
