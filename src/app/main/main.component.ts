import {Component, OnInit, ViewChild} from '@angular/core';
import {Challenge} from '../model/Challenge';
import {UserService} from '../user.service';
import {ChallengeService} from '../challenge.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  lgOpen = false;

  private str1: string;
  private str2: string;
  private str3: string;
  private numbers: number[];
  private myChallengesList: Challenge[];

  constructor(private userService: UserService, private challengeService: ChallengeService) {
  }

  ngOnInit() {
    // tslint:disable-next-line:max-line-length
    this.str1 = 'Зарегистрироваться и сдать экзамен на сертификат OCP по Java SE8';
    this.str2 = 'Пробежать марафон не медленнее, чем за 5 часов';
    this.str3 = 'Занять первое место в хакатоне ДКС';
    // @ts-ignore
    this.numbers = Array(9).fill().map((x, i) => i); // [0,1,2,3,4]

    this.userService.listChallenges().subscribe(data => this.myChallengesList = data);
  }

  createGame() {
    this.lgOpen = true;
  }

  myChallenges() {
    return this.myChallengesList;
  }
}
